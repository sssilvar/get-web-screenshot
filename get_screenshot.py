import os
import sys
from os.path import dirname, join, realpath

import pandas as pd

import selenium.webdriver as webdriver
from selenium.webdriver.firefox.options import Options
import contextlib


@contextlib.contextmanager
def quitting(thing):
    yield thing
    thing.quit()


current_dir = dirname(realpath(__file__))
driver_path = join(current_dir, 'drivers/')
os.environ['PATH'] = '{driver_path}:{path}'.format(driver_path=driver_path, path=os.environ['PATH'])
os.environ['http_proxy'] = ""
os.environ['https_proxy'] = ""

if __name__ == '__main__':
    # Load DOI list
    df = pd.read_csv('doi_list.csv', index_col=0)

    # Load options
    options = Options()
    options.headless = True

    with quitting(webdriver.Firefox(options=options)) as driver:
        # Iterate over all the DOIs
        for i, row in df.iterrows():
            # Get DOI value
            doi = row['DOI']
            # Download screenshot
            driver.implicitly_wait(10)
            driver.get('https://doi.org/{doi}'.format(doi=doi))

            # Add to dataframe
            print(driver.title)
            df.loc[i, 'Title'] = driver.title

            driver.save_screenshot('./images/{index}_{name}.png'.format(index=i, name=driver.title.replace(' ', '_')))
    df.to_csv('doi_list_and_titles.csv')
